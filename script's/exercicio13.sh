#!/bin/bash

# HIAGO NATAN FERNANDES DE SOUSA - MAT.: 114210178
# ADMINISTRACAO DE SISTEMAS - 2016.1

if [ $1 = "-e" ] && [ ! $2 ] && [ ! $3 ]; then
	isFileInDirectory=$(-e $3$2)
	if [ $isFileInDirectory ]; then
		echo "O arquivo existe no diretorio!"
	else 
		cp $2 $3
		echo "Arquivo copiado para o diretorio!"
	fi
if [ $1 = "-s" ] && [ ! $2 ] && [ ! $3 ]; then
	isFileInDirectory=$(-e $3$2)
	for arquivo in $3; do
		if [ arquivo ]; then
			isFileInDirectory=$(-e $3$2)
			if [ $isFileInDirectory ]; then
				echo "Arquivo existe no diretorio"
			else 
				cp $2 $3
				echo "Arquivo copiado para o diretorio!"
			fi
		fi
	done 
if [ $1 = "-d" ] && [ ! $2 ] && [ ! $3 ]; then	
	isFileInDirectory=$(-e $3$2)
	if [ $isFileInDirectory ]; then
		rm $2
		echo "Arquivo removido!"
	else
		echo "Arquivo nao existe no diretorio"
	fi
if [ $1 = "-c" ] && [ ! $2 ] && [ ! $3 ]; then
	for palavra in $3; do
		if [ $2 = palavra ]; then
			echo "A palavra existe no arquivo"
		fi
fi
		
